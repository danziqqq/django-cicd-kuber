FROM python:3
RUN mkdir /code && pip install -r requirements.txt
WORKDIR /code
COPY . /code/
ENTRYPOIN ["command", "args"]
CMD  ["python3 manage.py", "runserver 0.0.0.0:8000"]
